import React from "react"

const ItemList = (props) => {
  const itemCount = props.itemCount;
  const listItems = [];
  for (let i = 0; i < itemCount; ++i) {
    const inputText = `List Title ${i + 1}`;
    listItems[i] = <li><input type='text' value={inputText}/></li>;
  }

  return (
      <ul>{listItems}</ul>
  );
};

export default ItemList;