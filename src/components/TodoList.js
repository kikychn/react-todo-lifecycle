import React, {Component} from 'react';
import './todolist.less';
import ItemList from "./ItemList";

class TodoList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      itemCount: 0,
      items:[]
    };

    this.addItem = this.addItem.bind(this);

    console.log('constructor');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  render() {
    console.log('render');
    if (this.props.showState === 'Show') {
      return null
    }
    return (
        <div>
          <button onClick={this.addItem}>Add</button>
          <ItemList itemCount={this.state.itemCount} />
        </div>
    );
  }

  addItem() {
    this.setState({itemCount: this.state.itemCount + 1});
  }
}

export default TodoList;