import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      show: 'Show'
    };

    this.switchState = this.switchState.bind(this);
  }

  render() {
    return (
        <div className='App'>
          <button onClick={this.switchState}>{this.state.show}</button>
          <button onClick={this.refresh.bind(this)}>Refresh</button>
          <TodoList showState={this.state.show}/>
        </div>
    );
  }

  switchState() {
    this.state.show === 'Show' ? this.setState({show:'Hide'}):this.setState({show:'Show'});
  }

  refresh() {
    location.reload();
  }
}

export default App;